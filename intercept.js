function wrap(requestDetails) {
  return browser.storage.local.get("useragent").then((data) => {

    if(!data || !data.useragent) return requestDetails;

    let useragent = data.useragent;

    for (var i = 0; i < requestDetails.requestHeaders.length; i++) {
      var header = requestDetails.requestHeaders[i];
      if (header.name.toLowerCase().localeCompare("user-agent") === 0) {
        requestDetails.requestHeaders[i].value = useragent;
        found = true;
        break;
      }
    }

    if (!found) {
      requestDetails.requestHeaders.push({
        name: "User-Agent",
        value: useragent
      });
    }


    return requestDetails;
  });
}

browser.webRequest.onBeforeSendHeaders.addListener(
  wrap,
  { urls: ["<all_urls>"] },
  ["requestHeaders", "blocking"]
);
