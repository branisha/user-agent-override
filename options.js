document.querySelector("#save").addEventListener("click", (event) => {
  event.preventDefault();
  let value = document.querySelector("#agent").value;
  if (value) {
    browser.storage.local.set({
      useragent: value.trim(),
    });

    document.querySelector("#agent").value = value.trim();
  }
});

document.querySelector("#clear").addEventListener("click", (event) => {
  event.preventDefault();
  browser.storage.local.clear().then(() => document.querySelector("#agent").value = '');
});

browser.storage.local.get().then((data) => {
  if (data && data.useragent) {
    document.querySelector("#agent").value = data.useragent;
  }
});
